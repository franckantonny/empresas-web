export const TOKEN_KEY = "@airbnb-Token";
export const isAuthenticated = () => localStorage.getItem(TOKEN_KEY) !== null;
export const getToken = () => localStorage.getItem(TOKEN_KEY);
export const login = (token, client, uid) => {
  localStorage.setItem(TOKEN_KEY, token);
  localStorage.setItem("client", client);
  localStorage.setItem("uid", uid);
};