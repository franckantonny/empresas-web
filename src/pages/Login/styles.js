import styled from "styled-components";

export const Container = styled.div`
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
`;

export const Form = styled.form`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    img {
        margin-top: 3em;
        object-fit: contain;
        width: 18em;
    }
    #erro {
        margin-top: 0.7em;
        height: 4em;
    }
    h3 {
        width: 8em;
        height: 3em;
        font-size: 24px;
        font-weight: bold;
        text-align: center;
        color: #383743;
    }
    p {
        margin-bottom: 2em;
        width: 16em;
        font-size: 18px;
        line-height: 1.44em;
        letter-spacing: -0.25px;
        text-align: center;
        color: rgb(56,55,67);
    }
    input {
        margin-bottom: 1em;
        padding: 0.6em;
        padding-left: 2em;
        display: block;
        width: 25em;
        border: none;
        border-bottom: 1px solid rgb(56,55,67,0.5);
        background-repeat: no-repeat;
        background-color: transparent;
    }
    input::placeholder{
        font-size: 18px;
        letter-spacing: -0.25px;
        color: rgb(56,55,67,0.5);
    }
    button {
        width: 17em;
        height: 3em;
        border: none;
        border-radius: 4px;
        background-color: #57bbbc;
        color: #ffffff;
        font-size: 18px;
        font-weight: bold;
        margin-top: 1em;
        margin-bottom: 2em;
    }
`;