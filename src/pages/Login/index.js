import React, { Component } from "react";
import { withRouter } from "react-router-dom";


import api from "../../services/api";
import { login } from "../../services/auth";

import Logo from "../../assets/logo-home@3x.png";
import Email from "../../assets/ic-email.png";
import Cadeado from "../../assets/ic-cadeado.png";

import { Form, Container } from "./styles";

class SignIn extends Component {
  state = {
    email: "",
    password: "",
    error: ""
  };

  handleSignIn = async e => {
    e.preventDefault();
    const { email, password } = this.state;
    if (!email || !password) {
      this.setState({ error: "Preencha e-mail e senha para continuar!" });
    } else {
      try {
        const response = await api.post("/api/v1/users/auth/sign_in", { email, password });
        login(response.headers['access-token'],response.headers['client'], response.headers['uid']);
        this.props.history.push("/home");
      } catch (err) {
        console.log(err);
        this.setState({
          error:
            "Houve um problema com o login, verifique suas credenciais."
        });
      }
    }
  };

  render() {
    return (
      <Container>
        <Form onSubmit={this.handleSignIn}>
          <img src={Logo} alt="Logo Ioasys" />
          <div id="erro">
            {this.state.error && <p>{this.state.error}</p>}
          </div>
          <h3>BEM-VINDO AO EMPRESAS</h3>
          <p>Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</p>
          <input
            type="email"
            placeholder="E-mail"
            onChange={e => this.setState({ email: e.target.value })}
            style={{ backgroundImage: `url(${Email})` }}
          />
          <br />
          <input
            type="password"
            placeholder="Senha"
            onChange={e => this.setState({ password: e.target.value })}
            style={{ backgroundImage: `url(${Cadeado})` }}
          />
          <br />
          <button type="submit">ENTRAR</button>
        </Form>
      </Container>
    );
  }
}

export default withRouter(SignIn);