import React, { Component } from 'react';
import Pesquisa from '../../assets/ic-search-copy@3x.png';
import { Imagem } from './styles';

class Search extends Component {
    render() {
        return (
            <Imagem src={Pesquisa} alt='pesquisa'/>
        );
    }
}

export default Search;