import React, { Component } from 'react';
import Logo from '../../assets/logo-nav@3x.png';
import Search from '../Components/search';
import { Container } from "./styles";

class Home extends Component {

    state = {
        pesquisa: true,
        pesquisatext: false,
        pesquisar: "",
    };

    render() {
        console.log(this.state.pesquisar)
        return (
            <Container>
                <div>
                    <div id='logo'>
                        {this.state.pesquisa ? <img src={Logo} alt="Logo_ioasys" id='teste' /> : null}
                    </div>
                    <div id='pesquisa'>
                        <button onClick={() => this.state.pesquisa ? this.setState({ pesquisa: false }) : this.setState({ pesquisa: true })}></button>
                        <Search />
                        {this.state.pesquisatext ? <input type="text" onChange={(e) => this.setState({ pesquisatext: e.target.value })} /> : null}
                    </div>
                </div>
                <div id="results">
                    <p>Clique na busca para iniciar.</p>
                </div>
            </Container>
        );
    }
}

export default Home;