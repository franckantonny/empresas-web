import styled from 'styled-components';

export const Container = styled.div`
    div{
        justify-content: center;
        display: flex;
        background-color: #ee4c77;
        padding: 1em;
    }
    #teste{
        width: 15em;
    }
    #logo {
        width: 60%;
        justify-content: flex-end;
    }
    #results{
        height: 100%;
        text-align: center;
        background-color: #ebe9d7;
    }
    p{
        position: relative;
        margin-top: 15em;
        width: 100%;
    }

    #pesquisa{
        width: 40%;
        justify-content: flex-end;
    }

    input{
        margin-bottom: 1em;
        padding: 0.6em;
        padding-left: 2em;
        display: block;
        width: 25em;
        border: none;
        border-bottom: 1px solid #fff;
        background-repeat: no-repeat;
        background-color: transparent;
    }
    button{
        position: absolute;
        padding: 2em;
        background-color: transparent;
        border: none;
    }
`;